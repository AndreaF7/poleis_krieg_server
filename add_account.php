<?php
require_once("bootstrap.php");
$account = json_decode(file_get_contents("php://input"), true)[0];
$result = $dbh->getAccount($account["username"]);
if(empty($result)){
    $tableName = "AccountEntity";
    $dbh->create_table($account, $tableName);
    echo json_encode(array(array("result"=>1)));
}else{
    echo json_encode(array(array("result"=>-1)));
}
?>