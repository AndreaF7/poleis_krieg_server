<?php

class DatabaseHelper{
    private $db;
    
    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if($this->db->connect_error){
            die("Connesione al db fallita ");
        }
    } 
    
    public function getUserGames($userId){
        $tables = array("GameEntity", "PlayerEntity", "CaseEntity", "StructureEntity", "UnitEntity", "ResourceEntity", "SkillTreeAttributeEntity");
        $stmt = $this->db->prepare("SELECT DISTINCT gameId FROM PlayerEntity WHERE playerAccountId = ?");
        $stmt->bind_param("s", $userId);
        $stmt->execute();
        $result = $stmt->get_result();
        $ids = $result->fetch_all(MYSQLI_ASSOC);
        $result = array();
        foreach($ids as $idArray){
            $gameId = $idArray["gameId"];
            $result[] = $this->getGameInfo($gameId, $tables);
        }
        return $result;
    }

    public function getAccount($username){
        $stmt = $this->db->prepare("SELECT username FROM AccountEntity WHERE username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_row();
    }

    public function logIn($username, $password){
        $stmt = $this->db->prepare("SELECT username FROM AccountEntity WHERE username = ? AND `password` = ?");
        $stmt->bind_param("ss", $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_row();
    }
    
    
    public function getGameInfo($gameId, $tables){
        $res = array();
        foreach ($tables as $table) {
            $idName = $table == "GameEntity" ? "id" : "gameId";
            $stmt = $this->db->prepare("SELECT * FROM $table WHERE $idName = ?");
            $stmt->bind_param("i", $gameId);
            $stmt->execute();
            $result = $stmt->get_result();
            if($table == "GameEntity"){
                $assocArray = $result->fetch_all(MYSQLI_ASSOC)[0];
                $assocArray["onlineId"] = $assocArray["id"];
                $assocArray["id"] = 0;
                $res[$table] = $assocArray;
            } else{
                $assocArrays = $result->fetch_all(MYSQLI_ASSOC);
                for ($i=0; $i < count($assocArrays); $i++) { 
                    $assocArrays[$i]["gameId"] = 0;
                }
                $res[$table] = $assocArrays;
            }
        }
        return $res;
    }
    
    public function getNewGameId(){
        $stmt = $this->db->prepare("SELECT id FROM GameEntity ORDER BY id DESC LIMIT 1");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_array()[0] + 1;
    }
    
    public function create_table($tableInfo, $tableName){
        
        $values = "";
        $fields = "";
        $a_params = array();
        $param_type = "";
        $first = true;
        
        /* with call_user_func_array, array params must be passed by reference */
        foreach($tableInfo as $columnName => $columnValue){
            if(gettype($columnValue) == "string") $param_type .= "s";
            elseif (gettype($columnValue) == "integer") $param_type .= "i";
            elseif (gettype($columnValue) == "double") $param_type .= "d";
            else $param_type .= "i";
        }
        $a_params[] = & $param_type;
        foreach($tableInfo as $columnName => $columnValue){
            /* with call_user_func_array, array params must be passed by reference */
            $a_params[] = & $tableInfo[$columnName];    
            //echo print_r($a_params);
            if($first == true){
                $first = false;
                $fields .= $columnName;
                $values .= "?";
            } else{
                $fields .= ",".$columnName;
                $values .= ",?";
            }
        }
        
        $query = "INSERT INTO $tableName ($fields) VALUES ($values)";
        /* Prepare statement */
        $stmt = $this->db->prepare($query);
        //echo $query;
        if($stmt === false) {
            print_r($a_params);
            trigger_error('Wrong SQL: ' . $query .' Error: ' . $this->db->errno . ' ' . $this->db->error, E_USER_ERROR);
        }
        
        /* use call_user_func_array, as $stmt->bind_param('s', $param); does not accept params array */
        call_user_func_array(array($stmt, 'bind_param'), $a_params);
        
        $stmt->execute();
    }
    
    public function deleteTable($tableName, $gameId){
        $idName = $tableName == "GameEntity" ? "id" : "gameId";
        
        $query = "DELETE FROM $tableName WHERE $idName=?";
        $stmt = $this->db->prepare($query);
        if($stmt === false) {
            trigger_error('Wrong SQL: ' . $query .' Error: ' . $this->db->errno . ' ' . $this->db->error, E_USER_ERROR);
        }    
        $stmt->bind_param("i", $gameId);
        $stmt->execute();
        $stmt->execute();
    }
    
    public function createGame($gameId, $gameInfo){
        //clean game data with same id
        $this->deleteTable("GameEntity", $gameId);
        foreach ($gameInfo as $tableName => $raws) {
            $idName = ($tableName == "GameEntity" ? "id" : "gameId");
            if($tableName == "GameEntity"){
                $raws[$idName] = $gameId;
                unset($raws["onlineId"]);
                $this->create_table($raws, $tableName);
            } else {
                foreach ($raws as $raw) {
                    $raw[$idName] = $gameId;
                    $this->create_table($raw, $tableName);
                }
            }
        }
    }
    
    
    public function updateGameInfo($gameId, $gameInfo){//TODO test
        foreach ($gameInfo as $tableName => $raws) {
            $idName = $tableName == "GameEntity" ? "id" : "gameId";
            foreach ($raws as $raw) {
                foreach($raw as $column){
                    $stmt = $this->db->prepare("UPDATE $table SET $column = ? WHERE $idName = ?");
                    if(gettype($column) == "string") $stmt->bind_param("si", $gameId);
                    else $stmt->bind_param("ii", $gameId);
                    $stmt->execute();
                }
            }
        }
        
    }    
}
?>